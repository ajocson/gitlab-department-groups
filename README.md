# GitLab Department Groups

This Python script builds and prints a department's list of groups and its' members.

Each group consists of an object with the following keys:
- **`group_name`**: Name of department group.
- **`group_managers`**: List of group managers.
- **`group_members`**: List of group members.

So far this script only works with the `eng-dev` department. This script will require more testing for other departments and their YAML data.

**Example Output:**
```
[
    {
        'group_name': 'eng-dev-manage-access',
        'group_managers': [
            {'member_name': 'Liam McAndrew', 'member_handle': 'lmcandrew',
            {'member_name': 'Dennis Tang', 'member_handle': 'dennis'}
        ],
        'group_members': [
            {'member_name': 'Melissa Ushakov', 'member_handle': 'mushakov',
            {'member_name': 'Cormac Foster', 'member_handle': 'cfoster3'},
            {'member_name': 'Suri Patel', 'member_handle': 'suripatel'},
            {'member_name': 'Blair Lunceford'},
            {'member_name': 'Sanad Liaquat', 'member_handle': 'sliaquat'},
            {'member_name': 'Mike Long', 'member_handle': 'mikelong'},
            {'member_name': 'Daniel Mora', 'member_handle': 'dmoraBerlin'},
            {'member_name': 'Anne Lasch', 'member_handle': 'alasch'},
            {'member_name': 'Mike Jang', 'member_handle': 'mjang1'},
            {'member_name': 'Axil'},
            {'member_name': 'Ron Chan', 'member_handle': 'rchan-gitlab'}
        ]
    },
    {
        'group_name': 'eng-dev-manage-compliance',
        'group_managers': [
            {'member_name': 'Dan Jensen', 'member_handle': 'djensen'},
            {'member_name': 'Dennis Tang', 'member_handle': 'dennis'}
        ],
        'group_members': [
            {'member_name': 'Matt Gonzales', 'member_handle': 'mattgonzales'},
            {'member_name': 'Saumya Upadhyaya', 'member_handle': 'supadhyaya'},
            {'member_name': 'Suri Patel', 'member_handle': 'suripatel'},
            {'member_name': 'Mike Long', 'member_handle': 'mikelong'},
            {'member_name': 'Austin Regnery', 'member_handle': 'aregnery'},
            {'member_name': 'Anne Lasch', 'member_handle': 'alasch'},
            {'member_name': 'Mike Jang', 'member_handle': 'mjang1'},
            {'member_name': 'Axil'},
            {'member_name': 'Ron Chan', 'member_handle': 'rchan-gitlab'}
        ]
    },
    ...
]
```

## How to use
### config.yml
This project uses a configuration file to store and access variables.

**`gitlab_departments`**

The script will iterate through all of the departments under `gitlab_departments`. Each department should include a link to their team repos, which consists of a YAML file with group and member data. 

The script relies on the URLs because the YAML data is frequently updated. So, instead of uploading a new YAML file to get new department group members, this script will make a `request` to the URL.

**`team_metadata`**

Link to the directory of team members, which includes member metadata. Used for getting the members' GitLab handles.

**`job_roles`**

Each member in a group has a job role. The script will iterate through the list of roles to grab the member's full name.

### Running the script
```
pip install requirements.txt

python gitlab_department_groups.py
```


