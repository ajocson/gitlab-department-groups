#!/usr/bin/env python3

from yaml import safe_load
import requests
from pprint import pprint


config_yml = safe_load(open('config.yml')) # Load config.yml
job_roles = config_yml['job_roles'] # Get job_roles

def request_yaml(url):
    '''
    Retrieve YAML data from a URL and return it as a Python object.
    '''
    try:
        response = requests.get(url)
        if response.status_code == 200:
            yaml_file = safe_load(response.text)
            return yaml_file
        else:
            print(f'ERROR: {response.status_code}')
    except Exception as e:
        print(f'ERROR: {e}')


def get_gitlab_handle(team_metadata, user):
    '''
    Check if user has a GitLab handle listed in the team's metadata.

    If GitLab handle found, return object that includes 'member_name'
    AND 'member_handle.' Otherwise, return 'member_name.'
    '''

    # Some users have 'Interim' in their names. Remove this string.
    user = user.replace(' (Interim)', '')

    # Iterate through the metadata list and check if user exists.
    # If user matches the 'name' key, check if 'gitlab' key exists.
    # If so, return object that includes 'member_handle.'
    for member in team_metadata:
        if user in member['name']:
            if 'gitlab' in member:
                member_handle = member['gitlab']
                return {
                    'member_name': user,
                    'member_handle': member_handle
                }
            else:
                continue
        else:
            continue
    return { 'member_name': user }


def build_department_groups(department):
    '''
    Retrieve YAML data for 'department' and build a list of department group objects
    that include:
        - group_name
        - group_managers
        - group_members
    '''
    department_groups = []
    department_yml = request_yaml(config_yml['gitlab_departments'][department])
    team_metadata = request_yaml(config_yml['team_metadata'])

    # Iterate through the department's list of stages and groups
    for stage in department_yml['stages']:
        for group in department_yml['stages'][stage]['groups']:
    
            group_name = f'{department}-{stage}-{group}'
            group_managers = []
            group_members = []
            group_roles = department_yml['stages'][stage]['groups'][group]

            # Create member lists by getting values of specific job_roles.
            for role in job_roles:
                if (role in group_roles) and \
                    (group_roles[role]) and \
                    (group_roles[role] not in ['', 'TBD', ['TBD']]): # Skip 'TBD' values.

                        # If role includes 'manager' add user to group_managers
                        if 'manager' in role:
                            group_managers.append(group_roles[role])
                        # If role has a list of users, extend group_members list.
                        elif isinstance(group_roles[role], list):
                            group_members.extend(group_roles[role])
                        # Otherwise, add user to group_members
                        else:
                            group_members.append(group_roles[role])
                else:
                    continue
            
            # Go through the member lists and check for GitLab handles
            gitlab_group_managers = [get_gitlab_handle(team_metadata, user) for user in group_managers]
            gitlab_group_members = [get_gitlab_handle(team_metadata, user) for user in group_members]

            department_groups.append(
                {
                    'group_name': group_name,
                    'group_managers': gitlab_group_managers,
                    'group_members': gitlab_group_members,
                }
            )

    return department_groups


def main():
    # Go through the list of GitLab departments and print out department groups.
    for department in config_yml['gitlab_departments']:
        pprint(build_department_groups(department), sort_dicts=False, width=100)

if __name__ == '__main__':
    main()
